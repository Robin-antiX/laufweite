# laufweite

## Description
Command line tool for calculation of the tracking of a given textstring of given font and given font size on a given screen, output value in pixel.

## Installation
There is a debian installer package available, for either x32 or x64 architecture.

- Download the .deb file according to your system architecture from within the .deb-installer subdirectory.
- Download the respective checksum file.
- Check the download before installing, e.g. for 32 bit:
```
$ shasum -c laufweite-x32.deb.sha512.sum
OK
```

Only proceed if the command returns a clean OK, otherwise the download was damaged.

If the downloaded .deb file was checked OK, 

```
$ sudo apt-get update
$ sudo apt-get install ./laufweite-x32.deb
```

## Usage:

Call laufweite from command line, using its mandatory arguments.

```
laufweite Fontname:size=xy "text-string"
```

## Examples:

```
$ laufweite "Courier New:size=14" 'the quick brown fox jumps over the lazy dog'
473

$ laufweite "Times New Roman:size=14" 'the quick brown fox jumps over the lazy dog'
329

$ laufweite "Ubuntu Regular:size=14" 'the quick brown fox jumps over the lazy dog'
418

$ laufweite "Ubuntu Condensed:size=14" 'the quick brown fox jumps over the lazy dog'
308

$ laufweite "FreeSerif:size=14" 'the quick brown fox jumps over the lazy dog'
332

$ laufweite "FreeSerif:size=22" 'the quick brown fox jumps over the lazy dog'
519
```

## Target use case:
Tool for the proper calculation of window size in e.g. yad- or gtk- dialogs in shell scripts (e.g. bashscript)


## Compile instructions for antiX (only needed If you can't use the installer packages)

- Prerequisites:
```
$ sudo apt-get install pkg-config libxft-dev
```

- Compile:
```
$ cc laufweite.c -o laufweite `pkg-config --cflags x11 xft freetype2` `pkg-config --libs x11 xft freetype2`
```

- Create English localisation template:
```
$ xgettext -k_ -o laufweite.pot -L C --from-code=UTF-8 laufweite.c
```
Compilation tested on antiX 22 full (32bit) and antiX 23 full (64bit)


## Support
Questions, suggestions, and bug reporting please to https://www.antixforum.com

## Contributing
Improvement always welcome.

## Authors and acknowledgment
antiX community

## Project status
early testing phase

**License:**
GPL Version 3 (GPLv3)

------
Robin.antiX, 2023
