/* laufweite */
/* Version 0.2 */
/* Das Programm „laufweite” gibt die genaue Länge in Pixeln, die */
/* eine gegebene Zeichenkette in einer bestimmten Schriftart mit einer *
/* bestimmten Schriftgröße auf dem aktuellen Bildschrim mit der aktuellen */
/* Bildschirmauflösung hat, auf der Konsole als Zahlenwert aus.*/
/* 2023 antiX community*/
/* GPL v.3 */

#include <stdio.h>
#include <err.h>
#include <X11/Xft/Xft.h>
#include <libintl.h>
#include <locale.h>

#define _(String) gettext(String)

int main(int argc, char **argv) {

    /* Einrichten der i18n Umgebung */
    setlocale (LC_MESSAGES, "");
    bindtextdomain ("laufweite", "/usr/share/locale");
    textdomain ("laufweite");    

    /* Bildschirminformation auslesen, ggf. Fehlermeldung ausgeben und Programm beenden */
    Display *Bildschirm = XOpenDisplay(getenv("DISPLAY"));
    if (!Bildschirm) errx(1, _("Bildschirm %s konnte nicht geöffnet werden."), getenv("DISPLAY"));

    /* Befehlszeilenargumente prüfen, ggf. Fehlermeldung ausgeben und Programm beenden */
    if (argc != 3) errx(1, _("Aufruf: laufweite Schriftart[:Größe=Größe] 'Zeichenkette'"));

    /* System nach angegebener Schriftart durchsuchen, ggf. Fehlermeldung ausgeben und Programm beenden */
    XftFont *Schriftart = XftFontOpenName(Bildschirm, DefaultScreen(Bildschirm), argv[1]);
    if (!Schriftart) errx(1, _("Schriftart nicht gefunden"));

    /* Abrufen der Schriftartinformationen */
    XGlyphInfo Ergebnis;

    /* Länge der Zeichenkette in Pixeln errechnen */
    XftTextExtentsUtf8(Bildschirm, Schriftart, (FcChar8*)argv[2], strlen(argv[2]), &Ergebnis);

    /* Ausgabe des errechneten Wertes auf der Konsole */
    printf("%d\n", Ergebnis.xOff);

}
